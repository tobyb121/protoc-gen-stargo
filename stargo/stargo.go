package stargo

import (
	"fmt"
	"strings"

	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	"github.com/golang/protobuf/protoc-gen-go/generator"
)

func init() {
	s := new(stargo)
	s.generatedMapTypes = make(map[string]bool)
	generator.RegisterPlugin(s)
}

type stargo struct {
	gen               *generator.Generator
	generatedMapTypes map[string]bool
}

func (s *stargo) Name() string {
	return "stargo"
}

func (s *stargo) Init(g *generator.Generator) {
	s.gen = g
}

func (s *stargo) GenerateImports(file *generator.FileDescriptor) {
	s.P("import (")
	s.P(generator.GoImportPath("errors"))
	s.P()
	s.P(generator.GoImportPath("go.starlark.net/starlark"))
	s.P(generator.GoImportPath("go.starlark.net/syntax"))
	s.P(")")
	s.P()
	s.P("// reference syntax package (only used by enums)")
	s.P("var _ syntax.Token")
	s.P()
}

func (s *stargo) Generate(file *generator.FileDescriptor) {
	s.P()
	s.P("// Startgo Functions")
	s.P()

	for _, enum := range file.GetEnumType() {
		s.generateEnum(file, enum)
	}

	for _, msgType := range file.GetMessageType() {
		s.generateAssertion(file, msgType)
		s.generateType(file, msgType)
		s.generateFreeze(file, msgType)
		s.generateTruth(file, msgType)
		s.generateHash(file, msgType)
		s.generateAttr(file, msgType)
		s.generateAttrNames(file, msgType)
		for _, nested := range msgType.GetNestedType() {
			if nested.GetOptions().GetMapEntry() {
				s.generateMap(file, msgType, nested)
			}
		}
	}
}

func (s *stargo) P(str ...interface{}) {
	s.gen.P(str...)
}

func (s *stargo) In() {
	s.gen.In()
}

func (s *stargo) Out() {
	s.gen.Out()
}

func fqName(file *generator.FileDescriptor, typeName string) string {
	dottedPkg := "." + file.GetPackage()
	if dottedPkg != "." {
		dottedPkg += "."
	}
	return dottedPkg + typeName
}

func (g *stargo) objectNamed(fqName string) generator.Object {
	g.gen.RecordTypeUse(fqName)
	return g.gen.ObjectNamed(fqName)
}

func (g *stargo) typeName(str string) string {
	return g.gen.TypeName(g.objectNamed(str))
}

func (s *stargo) generateEnum(file *generator.FileDescriptor, enum *descriptor.EnumDescriptorProto) {
	enumType := s.typeName(fqName(file, enum.GetName()))
	s.P("func init(){")
	s.In()
	o := s.objectNamed(fqName(file, enum.GetName())).(*generator.EnumDescriptor)
	for _, e := range o.GetValue() {
		s.P("starlark.Universe[\"", strings.ToUpper(enumType+"_"+e.GetName()), "\"] = ", generator.CamelCaseSlice(o.TypeName())+"_"+e.GetName())
	}
	s.Out()
	s.P("}")
	s.P()
	s.P("var _ starlark.Value = (", enumType, ")(0)")
	s.P("var _ starlark.Comparable = (", enumType, ")(0)")
	s.P()
	s.P("func (m ", enumType, ") Type() string { return \"", enumType, "\"}")
	s.P()
	s.P("func (m ", enumType, ") Freeze() {}")
	s.P()
	s.P("func (m ", enumType, ") Truth() starlark.Bool { return starlark.True }")
	s.P()
	s.P("func (m ", enumType, ") Hash() (uint32, error) {")
	s.In()
	s.P("return uint32(m), nil")
	s.Out()
	s.P("}")
	s.P()
	s.P("func (m ", enumType, ") CompareSameType(op syntax.Token, y starlark.Value, depth int) (bool, error){")
	s.In()
	s.P("m2, ok := y.(", enumType, ")")
	s.P("if !ok {")
	s.In()
	s.P("return false, errors.New(\"invalid operand\")")
	s.Out()
	s.P("}")
	s.P("switch op {")
	s.P("case syntax.EQL:")
	s.In()
	s.P("return m == m2, nil")
	s.Out()
	s.P("case syntax.NEQ:")
	s.In()
	s.P("return m != m2, nil")
	s.Out()
	s.P("default:")
	s.In()
	s.P("return false, errors.New(\"invalid operator \"+op.String())")
	s.Out()
	s.P("}")
	s.Out()
	s.P("}")
}

func (s *stargo) generateAssertion(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto) {
	s.P("var _ starlark.Value = (*", s.typeName(fqName(file, msgType.GetName())), ")(nil)")
	s.P("var _ starlark.HasAttrs = (*", s.typeName(fqName(file, msgType.GetName())), ")(nil)")
	s.P()
}

func (s *stargo) generateType(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto) {
	s.P("func (m *", s.typeName(fqName(file, msgType.GetName())), ") Type() string {")
	s.In()
	s.P("return \"", msgType.GetName(), "\"")
	s.Out()
	s.P("}")
	s.P()
}

func (s *stargo) generateFreeze(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto) {
	s.P("func (m *", s.typeName(fqName(file, msgType.GetName())), ") Freeze() {}")
	s.P()
}

func (s *stargo) generateTruth(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto) {
	s.P("func (m *", s.typeName(fqName(file, msgType.GetName())), ") Truth() starlark.Bool {")
	s.In()
	s.P("if m != nil {")
	s.In()
	s.P("return starlark.True")
	s.Out()
	s.P("}")
	s.P("return starlark.False")
	s.Out()
	s.P("}")
	s.P()
}

func (s *stargo) generateHash(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto) {
	s.P("func (m *", s.typeName(fqName(file, msgType.GetName())), ") Hash() (uint32, error) {")
	s.In()
	s.P("return 0, errors.New(\"cannot hash type ", msgType.GetName(), "\")")
	s.Out()
	s.P("}")
	s.P()
}

func (s *stargo) isMap(field *descriptor.FieldDescriptorProto) bool {
	if field.GetType() != descriptor.FieldDescriptorProto_TYPE_MESSAGE {
		return false
	}
	if field.Label == nil || *field.Label != descriptor.FieldDescriptorProto_LABEL_REPEATED {
		return false
	}
	o := s.gen.ObjectNamed(field.GetTypeName()).(*generator.Descriptor)
	return o.GetOptions().GetMapEntry()
}

func isRepeated(field *descriptor.FieldDescriptorProto) bool {
	return field.Label != nil && *field.Label == descriptor.FieldDescriptorProto_LABEL_REPEATED
}

func (s *stargo) convertAttr(file *generator.FileDescriptor, fieldName string, field *descriptor.FieldDescriptorProto) string {
	switch field.GetType() {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		return fmt.Sprintf("starlark.Float(%s)", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		return fmt.Sprintf("starlark.Float(float64(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_INT64:
		return fmt.Sprintf("starlark.MakeInt64(%s)", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_UINT64:
		return fmt.Sprintf("starlark.MakeUint64(%s)", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_INT32:
		return fmt.Sprintf("starlark.MakeInt(int(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_UINT32:
		return fmt.Sprintf("starlark.MakeUint(uint(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_FIXED64:
		return fmt.Sprintf("starlark.MakeUint64(%s)", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_FIXED32:
		return fmt.Sprintf("starlark.MakeUint(uint(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		return fmt.Sprintf("starlark.Bool(%s)", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		return fmt.Sprintf("starlark.String(%s)", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_MESSAGE:
		return fmt.Sprintf("%s", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		return fmt.Sprintf("starlark.String(string(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_ENUM:
		return fmt.Sprintf("starlark.MakeInt(int(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_SFIXED32:
		return fmt.Sprintf("starlark.MakeInt(int(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_SFIXED64:
		return fmt.Sprintf("starlark.MakeInt64(%s)", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_SINT32:
		return fmt.Sprintf("starlark.MakeInt(int(%s))", fieldName)
	case descriptor.FieldDescriptorProto_TYPE_SINT64:
		return fmt.Sprintf("starlark.MakeInt64(%s)", fieldName)
	}
	s.gen.Fail("cannot convert type", field.GetType().String(), "for field", fieldName)
	return ""
}

func (s *stargo) generateAttr(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto) {
	s.P("func (m *", s.typeName(fqName(file, msgType.GetName())), ") Attr(attr string) (starlark.Value, error) {")
	s.In()
	s.P("switch attr {")
	for _, field := range msgType.GetField() {
		if field.GetType() == descriptor.FieldDescriptorProto_TYPE_ENUM {
			continue
		}

		fieldName := generator.CamelCase(field.GetName())
		s.P("case \"", field.GetName(), "\":")
		s.In()
		if isRepeated(field) {
			if s.isMap(field) {
				o := s.objectNamed(field.GetTypeName()).(*generator.Descriptor)
				fields := o.GetField()
				keyField := fields[0]
				valField := fields[1]

				keyGoType, _ := s.mappingKeyType(keyField)
				valGoType := s.mappingValueType(valField)

				mapName := s.mapTypeName(keyGoType, valGoType)
				s.P("return ", mapName, "(", s.convertAttr(file, "m."+fieldName, field), "), nil")
			} else {
				s.P("var result []starlark.Value")
				s.P("for _, val := range m.", fieldName, " {")
				s.In()
				s.P("result = append(result, ", s.convertAttr(file, "val", field), ")")
				s.Out()
				s.P("}")
				s.P("l := starlark.NewList(result)")
				s.P("l.Freeze()")
				s.P("return l, nil")
			}
		} else {
			s.P("return ", s.convertAttr(file, "m."+fieldName, field), ", nil")
		}
		s.Out()
	}
	s.Out()
	s.P("}")
	s.P("return nil, nil")
	s.Out()
	s.P("}")
	s.P()
}

func (s *stargo) generateAttrNames(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto) {
	s.P("func (m *", s.typeName(fqName(file, msgType.GetName())), ") AttrNames() ([]string) {")
	s.In()
	s.P("return []string{")
	s.In()
	for _, field := range msgType.GetField() {
		s.P("\"" + field.GetName() + "\",")
	}
	s.Out()
	s.P("}")
	s.Out()
	s.P("}")
	s.P()

}

func (s *stargo) mappingKeyType(field *descriptor.FieldDescriptorProto) (string, []string) {
	switch field.GetType() {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		return "float64", []string{
			"f, ok := key.(starlark.Float)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k := float64(f)",
		}
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		return "float32", []string{
			"f, ok := key.(starlark.Float)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k := float32(f)",
		}
	case descriptor.FieldDescriptorProto_TYPE_INT64,
		descriptor.FieldDescriptorProto_TYPE_SFIXED64,
		descriptor.FieldDescriptorProto_TYPE_SINT64:
		return "int64", []string{
			"i, ok := key.(starlark.Int)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k, ok := i.Int64()",
			"if !ok {",
			"	return nil, false, nil",
			"}",
		}
	case descriptor.FieldDescriptorProto_TYPE_UINT64,
		descriptor.FieldDescriptorProto_TYPE_FIXED64:
		return "uint64", []string{
			"i, ok := key.(starlark.Int)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k, ok := i.Uint64()",
			"if !ok {",
			"	return nil, false, nil",
			"}",
		}
	case descriptor.FieldDescriptorProto_TYPE_INT32,
		descriptor.FieldDescriptorProto_TYPE_SFIXED32,
		descriptor.FieldDescriptorProto_TYPE_SINT32:
		return "int32", []string{
			"i, ok := key.(starlark.Int)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"i64, ok := i.Int64()",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k := int32(i64)",
		}
	case descriptor.FieldDescriptorProto_TYPE_UINT32,
		descriptor.FieldDescriptorProto_TYPE_FIXED32:
		return "uint32", []string{
			"i, ok := key.(starlark.Int)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"u64, ok := i.Uint32()",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k := uint32(u64)",
		}
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		return "bool", []string{
			"b, ok := key.(starlark.Bool)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k := bool(b)",
		}
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		return "string", []string{
			"s, ok := key.(starlark.String)",
			"if !ok {",
			"	return nil, false, nil",
			"}",
			"k := string(s)",
		}
	}
	s.gen.Fail("invalid type for map")
	return "", nil
}

func (s *stargo) mappingValueType(field *descriptor.FieldDescriptorProto) string {
	switch field.GetType() {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		return "float64"
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		return "float32"
	case descriptor.FieldDescriptorProto_TYPE_INT64:
		return "int64"
	case descriptor.FieldDescriptorProto_TYPE_UINT64:
		return "uint64"
	case descriptor.FieldDescriptorProto_TYPE_INT32:
		return "int32"
	case descriptor.FieldDescriptorProto_TYPE_UINT32:
		return "uint32"
	case descriptor.FieldDescriptorProto_TYPE_FIXED64:
		return "uint64"
	case descriptor.FieldDescriptorProto_TYPE_FIXED32:
		return "uint32"
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		return "bool"
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		return "string"
	case descriptor.FieldDescriptorProto_TYPE_MESSAGE:
		return "*" + s.typeName(field.GetTypeName())
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		return "[]byte"
	case descriptor.FieldDescriptorProto_TYPE_ENUM:
		o := s.gen.ObjectNamed(field.GetTypeName())
		enum := o.(*generator.EnumDescriptor)
		return generator.CamelCase(enum.GetName())
	case descriptor.FieldDescriptorProto_TYPE_SFIXED32:
		return "int32"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED64:
		return "int64"
	case descriptor.FieldDescriptorProto_TYPE_SINT32:
		return "int32"
	case descriptor.FieldDescriptorProto_TYPE_SINT64:
		return "int64"
	}
	s.gen.Fail("invalid type for map")
	return ""
}

func sanitize(name string) string {
	return strings.TrimLeft(name, "*[]")
}

func (s *stargo) mapTypeName(keyType, valType string) string {
	return "starlarkMap_" + sanitize(keyType) + "_" + sanitize(valType)
}

func (s *stargo) generateMap(file *generator.FileDescriptor, msgType *descriptor.DescriptorProto, nested *descriptor.DescriptorProto) {
	fields := nested.GetField()
	keyField := fields[0]
	valField := fields[1]

	keyGoType, conversion := s.mappingKeyType(keyField)
	valGoType := s.mappingValueType(valField)

	mapName := s.mapTypeName(keyGoType, valGoType)

	if _, ok := s.generatedMapTypes[mapName]; ok {
		return
	}
	s.generatedMapTypes[mapName] = true

	s.P("type ", mapName, " map[", keyGoType, "]", valGoType)
	s.P()

	s.P("var _ starlark.Value = (", mapName, ")(nil)")

	s.P("func (m ", mapName, ") String() string          { return fmt.Sprint((map[", keyGoType, "]", valGoType, ")(m)) }")
	s.P("func (m ", mapName, ") Type()   string          { return \"dict\"}")
	s.P("func (m ", mapName, ") Freeze()                 {}")
	s.P("func (m ", mapName, ") Truth()  starlark.Bool   { return starlark.Bool(len(m) > 0) }")
	s.P("func (m ", mapName, ") Hash()   (uint32, error) { return 0, fmt.Errorf(\"unhashable type: dict\") }")
	s.P()

	s.P("var _ starlark.Mapping = (", mapName, ")(nil)")
	s.P()

	s.P("func (m ", mapName, ") Get(key starlark.Value) (starlark.Value, bool, error) {")
	s.In()
	for _, c := range conversion {
		s.P(c)
	}

	s.P("val, ok := m[k]")
	s.P("if !ok {")
	s.In()
	s.P("return nil, false, nil")
	s.Out()
	s.P("}")

	s.P("return ", s.convertAttr(file, "val", valField), ", true, nil")
	s.Out()
	s.P("}")
	s.P()
}
